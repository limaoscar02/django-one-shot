from django.shortcuts import render
from todos.models import TodoList
# Create your views here.


def todos_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list": todo_lists,
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):

    todolist = TodoList.objects.get(id=id)

    context = {
        "totdolistdetail": todolist
    }
    return render(request, "todos/todo_list_detail.html", context)


# def create_form(request, id):
#     create = TodoList.objects.get(id=id)
#     if request.method == "POST":
#         form = createforms(request.POST, isinstance=create)
#         if form.is_valid():
#             form.save()
#             redirect("todo_list")
#     else:
#         form = createforms(isinstance)

#     context = {
#         "create_form": TodoList.objects.all()
#     }

#     return render(request, "todos/create, context")
